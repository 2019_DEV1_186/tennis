//
//  Game.swift
//  KataTennis
//
//  Created by unknown on 18/07/2019.
//  Copyright © 2019 Unknown. All rights reserved.
//

import Foundation

class Game {
    private let playerOneName: String
    private let playerTwoName: String
    
    private var playerOneScore = 0
    private var playerTwoScore = 0
    
    init(playerOneName: String, playerTwoName: String) {
        self.playerOneName = playerOneName
        self.playerTwoName = playerTwoName
    }
    
    func isThereADeuce() -> Bool {
        return playerOneScore >= 3 && playerTwoScore == playerOneScore;
    }
    
    private func isThereAWinner() -> Bool {
        if(playerTwoScore >= 4 && playerTwoScore >= playerOneScore + 2) {
            return true
        }
        
        if(playerOneScore >= 4 && playerOneScore >= playerTwoScore + 2) {
            return true;
        }
        return false;
    }
    
    private func highestScorePlayer() -> String {
        if (playerOneScore > playerTwoScore) {
            return playerOneName
        } else {
            return playerTwoName
        }
    }
    
    private func isAhead() -> Bool {
        if (playerTwoScore >= 4 && playerTwoScore == playerOneScore + 1) {
            return true
        }
        if (playerOneScore >= 4 && playerOneScore == playerTwoScore + 1) {
            return true;
        }
        return false;
    }
    
    
    func performScoreByPlayerOne() {
        playerOneScore += 1
    }
    
    func performScoreByPlayerTwo() {
        playerTwoScore += 1
    }
    
    func calculateScore() -> String {
        if (isThereAWinner()) {
            return highestScorePlayer() + " wins";
        }
        
        if (isAhead()) {
            return "Advantage " + highestScorePlayer();
        }
        
        if (isThereADeuce()) {
            return "Deuce";
        }
        
        if (playerOneScore == playerTwoScore) {
            return describeScore(playerOneScore) + " all";
        }
        
        return describeScore(playerOneScore) + "," + describeScore(playerTwoScore);
    }
    
    private func describeScore(_ score: Int) -> String {
        switch score {
        case 0:
            return "Love"
        case 1:
            return "Fifteen"
        case 2:
            return "Thirty"
        case 3:
            return "Fourty"
        default:
            return "Unknown"
        }
    }
}
