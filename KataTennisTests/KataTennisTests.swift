//
//  KataTennisTests.swift
//  KataTennisTests
//
//  Created by unknown on 18/07/2019.
//  Copyright © 2019 Unknown. All rights reserved.
//

import XCTest
@testable import KataTennis

class KataTennisTests: XCTestCase {

    let game = Game(playerOneName: "John", playerTwoName: "Alex")
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testNewGameShouldReturnLoveAll() {
        let score = game.calculateScore();
        XCTAssertEqual(score, "Love all")
    }
    
    func testPlayerOneWinsFirstBall() {
        game.performScoreByPlayerOne();
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Fifteen,Love")
    }
    
    func testFifteenAll() {
        game.performScoreByPlayerOne();
        game.performScoreByPlayerTwo();
    
        let score = game.calculateScore();
        XCTAssertEqual(score, "Fifteen all")
    }
    
    func testPlayerTwoWinsFirstTwoBalls() {
        createScore(0, 2);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Love,Thirty")
    }
    
    func testPlayerOneWinsFirstThreeBalls() {
        createScore(3, 0);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Fourty,Love")
    }
    
    func testPlayersAreDeuce() {
        createScore(3, 3);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Deuce")
    }
    
    func testPlayerOneWinsGame() {
        createScore(4, 0);
    
        let score = game.calculateScore();
        XCTAssertEqual(score, "John wins")
    }
    
    func testPlayerTwoWinsGame() {
        createScore(1,4);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Alex wins")
    }
    
    func testPlayersAreDuce4() {
        createScore(4, 4);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Deuce")
    }
    
    func testPlayerTwoAdvantage() {
        createScore(4, 5);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Advantage Alex")
    }
    
    func testPlayerOneAdvantage() {
        createScore(5, 4);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Advantage John")
    }
    
    func testPlayerTwoWins() {
        createScore(2, 4);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Alex wins")
    }
    
    
    func testPlayerTwoWinsAfterAdvantage() {
        createScore(6, 8);
        
        let score = game.calculateScore();
        XCTAssertEqual(score, "Alex wins")
    }
    
    func testPlayerOneWinsAfterAdvantage() {
        createScore(8, 6);
        let score = game.calculateScore();
        XCTAssertEqual(score, "John wins")
    }
    
    func createScore(_ playerOneBalls: Int, _ playerTwoBalls: Int) {
        for _ in 0..<playerOneBalls {
            game.performScoreByPlayerOne();
        }
        for _ in 0..<playerTwoBalls {
            game.performScoreByPlayerTwo();
        }
    }
}
